# -*- coding: utf-8 -*-

from openerp import api, fields, models


class SaleOrderSequence(models.Model):
    _name = 'sale.order.sequence'
    _description = 'Module for two or more sequence in sale orders'

    name = fields.Char('Business Name', required=True)
    prefix = fields.Char('Prefix')
    sequence_id = fields.Many2one('ir.sequence',string='Sequence', \
        required=True)
    company_id = fields.Many2one('res.company', string='Company')


class SaleOrder(models.Model):
    _inherit = "sale.order"

    business_sequence_id = fields.Many2one('sale.order.sequence', \
        string='Source Business', required=True)

    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            business_id = vals.get('business_sequence_id', False)
            if business_id:
                vals['name'] = self.env['sale.order.sequence'].\
                    search([('id','=',business_id)]).sequence_id.next_by_id()
            else:
                vals['name'] = self.env['ir.sequence'].\
                    next_by_code('sale.order') or 'New'
        result = super(SaleOrder, self).create(vals)
        return result