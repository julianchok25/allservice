# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    John W. Viloria Amaris <john.viloria.amaris@gmail.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    "name":"Sale Order Sequence",
    "description":"""\
Sale Order Sequence
==================================

Módulo para brindar la posibilidad de tener varias sequencias de
cotizaciones dependiendo de la empresa que realice la cotización.
    """,
    "depends":['sale'],
    "category":"Sales",
    "author":"John Viloria Amaris",
    "data":[
    	"views/sale_order_view.xml",
        "views/sale_order_sequence_view.xml",
        "security/ir.model.access.csv",
    ],
    "installable":True
 }
