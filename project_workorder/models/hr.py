# -*- coding: utf-8 -*-

from openerp import fields, models

class EmployeeExpense(models.Model):
    _inherit = 'hr.expense'

    workorder_id = fields.Many2one('project.workorder', string='Work Order')