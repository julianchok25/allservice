# -*- coding: utf-8 -*-

from openerp import api, fields, models, _
from openerp.exceptions import ValidationError
import logging

_logger = logging.getLogger(__name__)

class ProjectWorkorder(models.Model):
    _name = 'project.workorder'
    _description = 'Work Order Implementation for project tasks'

    name = fields.Char('Name', required=True,
        default='New')
    issue_id = fields.Many2one('project.issue', string='Issue Id')
    date = fields.Date('Date', default=fields.Date.today())
    responsible_id = fields.Many2one('res.users', \
        string='Responsible', required=True)
    task_ids = fields.One2many('project.task', 'workorder_id', \
        string='Tasks')
    state = fields.Selection([
        ('new',         'New'),
        ('planned',     'Planned'),
        ('released',    'Released'),
        ('cancelled',   'Cancelled')
        ], string='State', default='new')
    tasks_finished = fields.Boolean('Tasks Finished',
        compute='_check_tasks_finished', default=0)
    project_id = fields.Many2one('project.project',
        related='issue_id.project_id', string='Project')
    issue_sec = fields.Char(related='issue_id.sec',
        string='Issue Sequence')
    expense_ids = fields.One2many('hr.expense','workorder_id',
        string='Expenses')

    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].\
                next_by_code('project.workorder')
        workorder = super(ProjectWorkorder, self).create(vals)
        return workorder

    @api.multi
    def action_planned(self):
        if len(self.task_ids) == 0:
            raise ValidationError(
                    _('The work order must have at least one task.')
                )
        self.state = 'planned'

    @api.multi
    def action_new(self):
        self.state = 'new'

    @api.multi
    def action_cancel(self):
        self.state = 'cancelled'

    @api.multi
    def action_released(self):
        self.state = 'released'

    @api.depends('task_ids')
    def _check_tasks_finished(self):
        if not self.task_ids:
            self.tasks_finished = False
            return
        for task in self.task_ids:
            if not task.finished:
                self.tasks_finished = False
                return
        self.tasks_finished = True

class ProjectIssue(models.Model):
    _inherit = "project.issue"

    workorder_ids = fields.One2many('project.workorder', \
        'issue_id', string='Work Orders')
    wo_count = fields.Integer('Work Order Count', compute='_wo_count',
        default=0)

    @api.depends('workorder_ids')
    def _wo_count(self):
        if self.workorder_ids:
            self.wo_count = len(self.workorder_ids)
        else:
            self.wo_count = 0

class ProjectTask(models.Model):
    _inherit = 'project.task'

    workorder_id = fields.Many2one('project.workorder', \
        string='Work Order Id')
    # issue_id = fields.Many2one('project.issue',
    #     string='Project Issue')
    partner_id = fields.Many2one('res.partner', 
        related='issue_id.partner_id', string='Client')
    project_id = fields.Many2one('project.project',
        related='issue_id.project_id', string='Project')
    issue_sec = fields.Char(related='issue_id.sec',
        string='Issue Sequence', readonly=True)
    issue_description = fields.Text(related='issue_id.description',
        string='Issue Description', readonly=True)
    location_id = fields.Char(related='issue_id.location_id.name',
        string='Location', readonly=True)
    location_type_id = fields.Char(related='issue_id.location_type_id.name',
        string='Location Type', readonly=True)
    city_id = fields.Char(related='issue_id.city_id.city',
        string='City', readonly=True)
    number_sap = fields.Char(related='issue_id.number_sap',
        string='Codigo Cliente', readonly=True)
    location_code = fields.Char(related='issue_id.location_id.code',
        string='Location Code', readonly=True)
    location_address = fields.Char(related='issue_id.location_id.address', string="Address", readonly=True)
    location_latitude = fields.Char(related='issue_id.location_id.latitude', string="Latitude", readonly=True)
    location_longitude = fields.Char(related='issue_id.location_id.longitude', string="Longitude", readonly=True)
    coordinate = fields.Char(string="Full Coordinate", compute='_get_concatenate_values', readonly=True)
    origin_address = fields.Char('Origin Technician Adress')
    origin_tech_coord = fields.Char(string="Origin Technician Coordinate", readonly=True)

    @api.depends('location_latitude','location_longitude')
    def _get_concatenate_values(self):
        for r in self:
            if(r.location_latitude and r.location_longitude):
                r.coordinate = r.location_latitude + ", " + r.location_longitude


class Project(models.Model):
    _inherit = 'project.project'

    use_workorders = fields.Boolean('Use Workorders', default=False)
    wo_count = fields.Integer('Work Order Count', compute='_wo_count',
        default=0)

    @api.depends('issue_ids')
    @api.one
    def _wo_count(self):
        wo_count = 0
        for issue in self.issue_ids:
            if issue.workorder_ids:
                wo_count += len(issue.workorder_ids)
        self.wo_count = wo_count