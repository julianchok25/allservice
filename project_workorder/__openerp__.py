# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    John W. Viloria Amaris <john.viloria.amaris@gmail.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    "name":"Project Work Order",
    "description":"""\
Project Work Order
==================

Implementación de la orden de trabajo para procesar las tareas
necesarias para atender las incidencias de los clientes.
    """,
    "depends":['project_issue_custom','project_task_custom','project'],
    "category":"Project",
    "author":"John Viloria Amaris",
    "data":[
        "views/project_workorder_view.xml",
        "views/project_issue_view.xml",
        "views/data.xml",
        "security/ir.model.access.csv",
        "views/project_task_view.xml",
        "views/project_dashboard.xml",
    ],
    "installable":True
 }
