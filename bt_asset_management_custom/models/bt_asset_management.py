# -*- coding: utf-8 -*-

from openerp import api, fields, models
import logging

_logger = logging.getLogger(__name__)

class ProjectEquipmentType(models.Model):
    _name = 'project.spare.equipment.type'
    _description = "Equipment Type" 

    name = fields.Char('Name', required=True)
    equipment_category = fields.Many2one('product.category',
        string='Equipment Category', required=True)
    spare_location_ids = fields.One2many(
        'project.equipment.spare.location','equipment_type',
        string='Spare Locations')

class ProjectEquipmentSpareLocation(models.Model):
    _name = 'project.equipment.spare.location'
    _description = "Equipment Location"

    name = fields.Char('Name', required=True)
    equipment_type = fields.Many2one('project.spare.equipment.type',
        string='Equipment Type', required=True)

class HeavyEquipmentCat(models.Model):
    _name = 'project.spare.category.line'
    _description = "Spare Category"

    product_categ = fields.Selection([
        ('heavy','Heavy Equipment'),
        ('lightweight','Lightweight Equipment')
        ], string='Equipment Category')
    equipment_type = fields.Many2one('project.spare.equipment.type',
        string='Equipment Type')
    spare_location = fields.Many2one('project.equipment.spare.location',
        string='Spare Location')
    product_id = fields.Many2one('product.template', 
        string="Product")
    product_categ_id = fields.Many2one(related='product_id.categ_id',
        string='Category')

    @api.onchange('equipment_type')
    def equipment_type_onchange(self):
        if not self.equipment_type:
            return
        ids = [location.id for location in \
            self.equipment_type.spare_location_ids]
        return {'domain':{
                'spare_location':[('id','in',ids)]
                }
        }

    @api.onchange('product_categ_id')
    def product_categ_id_onchange(self):
        equipment_types = self.env['project.spare.equipment.type'].\
            search([('equipment_category','=',self.product_categ_id.id)])
        ids = [etypes.id for etypes in equipment_types]
        return {'domain':{
                'equipment_type': [('id','in',ids)]
                }
        }

class ProductTemplate(models.Model):
    _inherit = 'product.template'

    equipment_cat_ids = fields.One2many('project.spare.category.line',
        'product_id', string='Equipment Categories')

class BtAsset(models.Model):
    _inherit = 'bt.asset'

    partner_id = fields.Many2one(related='location_id.partner_id',
        string='Partner', readonly=True)
    city_id = fields.Many2one(related='location_id.city_id', 
        string='City', readonly=True)
    location_type_id = fields.Many2one(related='location_id.location_type_id',
        string='Location Type', readonly=True)
    location_id = fields.Many2one('crm.customer.location',
        string='Location', required=True)
    equipment_category_id = fields.Many2one('product.category',
        string='Category')

class CrmCustomerLocation(models.Model):
    _inherit = 'crm.customer.location'

    @api.depends('asset_ids')
    def _assets_count(self):
        try:
            self.assets_count = len(self.asset_ids)
        except:
            self.assets_count = 0

    asset_ids = fields.One2many('bt.asset','location_id',
        string='Assets')
    assets_count = fields.Integer('Assets', compute=_assets_count)

