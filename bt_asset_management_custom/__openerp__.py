# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    John W. Viloria Amaris <john.viloria.amaris@gmail.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    "name":"Project Customer Assets Management",
    "description":"""\
Project Customer Assets Management
==================================

Ajustes al modulo bt_asset_management.
    """,
    "depends":['bt_asset_management','product','crm_customer'],
    "category":"Project",
    "author":"John Viloria Amaris",
    "data":[
        "security/ir.model.access.csv",
    	"views/asset_management_view.xml",
        "views/product_view.xml",
    ],
    "installable":True
 }
