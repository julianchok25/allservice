# -*- coding: utf-8 -*-

import openerp
from openerp.http import request
import openerp.http as http
import logging

_logger = logging.getLogger(__name__)

class SpareList(http.Controller):

    @http.route('/web/dataset/spare_list', type='json', auth='public')
    def spare_list(self, **post):
        # Ejemplo de peticion:
        # {"json-rpc": "2.0","method": "call","params": {"fields":["id","name","categ_id"],
        # "product_categ_id":9,"equipment_type_id":2,"spare_location_id":2,
        # "kwargs": {},"context":{}},"id": 10}
        params = request.params.copy()
        product_categ_id = params.get('product_categ_id', False)
        equipment_type_id = params.get('equipment_type_id', False)
        spare_location_id = params.get('spare_location_id', False)
        fields = params.get('fields', ['id'])
        Product = request.env['product.template']
        products = Product.sudo().search([('categ_id','=',product_categ_id)])
        records = []
        for product in products:
            for cat_line in request.env['project.spare.category.line']\
                .sudo().search([('product_id','=', product.id)]):
                if cat_line.equipment_type.id == equipment_type_id \
                and cat_line.spare_location.id == spare_location_id:
                    #REQ No. 107, 112
                    records.append({'product_id'  :product.id,
                                    'product_name':product.name,
                                    'location_id':cat_line.spare_location.id,
                                    'location_name':cat_line.spare_location.name,
                                    'equipment_id':cat_line.equipment_type.id,
                                    'equipment_name':cat_line.equipment_type.name,
                                    'categ_id':product.categ_id.id,
                                    'categ_name':product.categ_id.name,
                                    'product_image':product.image_medium})
                                    #END REQ 107, 112
        if not records:
            return {
                'length': 0,
                'records': []
            }
        
        return {'records': records,
                'length': len(records)}