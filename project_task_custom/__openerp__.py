# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    John W. Viloria Amaris <john.viloria.amaris@gmail.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    "name":"Project Task Customizations",
    "description":"""\
Project Task Customizations
==================================

Modificaciones varias a la gestión de tareas.
    """,
    "depends":['project_issue','project_task_materials_stock','product','mail',
    'bt_asset_management','bt_asset_management_custom'],
    "category":"Project",
    "author":"John Viloria Amaris",
    "data":[
        "views/project_task_view.xml",
        "views/product_view.xml",
        "views/data.xml",
        "security/ir.model.access.csv",
        "security/project_task_security.xml",
        "views/stock_view.xml",
        "views/task_report.xml",
        "report/report_task_report.xml",
        #"report/report_pos_receipt.xml",
        "views/bt_asset_view.xml",
        "views/templates.xml"
    ],
    "installable":True
 }
