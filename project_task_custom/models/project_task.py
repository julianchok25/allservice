# -*- coding: utf-8 -*-

from openerp import api, fields, models, _
from openerp.exceptions import ValidationError
import base64
import logging, requests , json, datetime

_logger = logging.getLogger(__name__)

class ProjectTask(models.Model):
    _inherit = 'project.task'

    def _get_source_location(self):
        try:
            location = self.env['stock.location'].\
                search([('material_source_location','=', True)])[0]
            return location.id
        except Exception as e:
            return False

    def _get_target_location(self):
        try:
            location = self.env['stock.location'].\
                search([('material_target_location','=', True)])[0]
            return location.id
        except Exception as e:
            return False

    user_id = fields.Many2one(domain=[('technician','=',True)],
        readonly=False)
    task_service_ids = fields.One2many('project.task.service',
        'task_id', string='Services',
        readonly=False)
    finished = fields.Boolean('Finished', default=False,
        readonly=False)
    customer_sign_image = fields.Binary('Functionary Sign', attachment=True,
        readonly=False)
    name = fields.Char(default='New')
    #customer_sign_image = fields.Text('Functionary Sign')
    customer_asset_ids = fields.One2many('project.customer.asset',
        'task_id', string='Assets',
        readonly=False)
    assignment_status = fields.Selection([
        ('partial', 'Partial Service'),
        ('correct', 'Corrected'),
        ('fail', 'Failed'),
        ('cancel', 'Cancelled')
        ], string='Assignment Status', default="partial",
        readonly=False)
    fail_cause_id = fields.Many2one('project.task.fail.cause',\
        string='Fail Cause',
        readonly=False)
    fail_description_id = fields.Many2one('project.task.fail.description',\
        string='Fail Description',
        readonly=False)
    fail_notes = fields.Text('Notes',
        readonly=False)
    location_source_id = fields.Many2one(default=_get_source_location,
        readonly=False)
    location_dest_id = fields.Many2one(default=_get_target_location,
        readonly=False)
    expense_ids = fields.One2many('hr.expense','task_id', string='Expenses',
        readonly=False)
    employee_id = fields.Many2one('hr.employee', string='Employee',
        readonly=False)
    functionary_name = fields.Char('Functionary Name',
        readonly=False)
    functionary_email = fields.Char('Functionary Mail',
        readonly=False)
    functionary_vat = fields.Char('Functionary VAT',
        readonly=False)
    notes = fields.Text(readonly=False)
    # REQ No. 91
    # workflow fields stages
    state = fields.Selection([
        ('draft', "Draft"),
        ('program', "Programmed"),
        ('valid', "Validated"),
        ('notif', "Notified"),
        ('cancel', "Cancelado"),
        ('closed', "Closed"),
    ], default='draft')

    #REQ No. 131
    issue_dfinish = fields.Date(related='issue_id.date_finish')
    date_deadline = fields.Date('Date Deadline', compute='_get_end_date', readonly=False)

    #REQ No. 110
    entry_time = fields.Datetime('Entry Time')
    departure_time = fields.Datetime('Departure Time')
    # entry_time = fields.Float('Entry Time', digits=(6,2))
    # departure_time = fields.Float('Departure Time', digits=(6,2))

    #REQ No. 131
    @api.depends('issue_dfinish')
    def _get_end_date(self):
        if self.issue_dfinish:
            self.date_deadline = self.issue_dfinish

    @api.multi
    def action_draft(self):
        self.state = 'draft'
    
    @api.multi
    def action_program(self):
        self.state = 'program'
    
    @api.multi
    def action_send_email(self):
        self.ensure_one()
        self._send_email('project_task_custom.email_template_project_task')
        self.one_signal()
        self.state = 'program'
    
    @api.multi
    def one_signal(self):

        player_id = self.user_id.player_id
        phone_user_id = self.env['res.users'].search([('player_id','=','player_id')])
        header = {"Content-Type": "application/json; charset=utf-8",
        "Authorization": "Basic NDFiOTk0OTQtNDE2Zi00OTBhLWI0YjEtMTU1YmRjYTIyY2Jk"}
        payload = {"app_id": "24193be6-3c15-4975-8f5c-102ea593a5a3",
        #"included_segments": ["All"],
        "include_player_ids": [player_id],
        "contents": {"en": "All Service Rhyno"}}
        req = requests.post("https://onesignal.com/api/v1/notifications", headers=header, data=json.dumps(payload))

    @api.multi
    def action_validated(self):
        self.state = 'valid'

    @api.multi
    def action_notified(self):
        #self.state = 'notif'
        # REQ No. 83
        self.ensure_one()
        ir_model_data = self.env['ir.model.data'].search([])

        try:
            template_id = ir_model_data.get_object_reference('project_task_custom', 'task_custom_template')[1]
        except ValueError:
            template_id = False
        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        ctx = dict(self.env.context or {})
        ctx.update({
            'default_model': 'project.task',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'mark_so_as_sent': True
        })

        return {
            'name': 'Compose Email',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }
        # END REQ No. 83
    
    @api.multi
    def action_cancel(self):
        self.state = 'cancel'
    
    @api.multi
    def action_closed(self):
        self.state = 'closed'
    # END REQ No. 91


    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].\
                next_by_code('project.task')
        task = super(ProjectTask, self).create(vals)
        return task

    @api.multi
    def write(self, vals):
        finished = vals.get('finished', False)
        res = super(ProjectTask, self).write(vals)
        manager_email = False
        if finished:
            try:
                manager_email = self.issue_id.user_id.partner_id.email
            except Exception as e:
                pass
            if manager_email:
                self.ensure_one()
                self._send_email\
                ('project_task_custom.email_template_finished_project_task')
        return res

    def _send_email(self, mail_template):
        try:
            template = self.env.ref(mail_template)
            template.send_mail(self.id, force_send=True)
            return True
        except ValueError:
            template_id = False
            return False

    @api.multi
    @api.onchange('assignment_status')
    def assignment_status_change(self):
        if not self.assignment_status:
            return
        assignment_status = self.assignment_status
        fails = self.env['project.task.fail.cause'].\
            search([('assignment_status','=',assignment_status)])
        fail_ids = [fail.id for fail in fails]
        return {'domain':{
            'fail_cause_id':[('id','in',fail_ids)]}
            }        

    @api.multi
    @api.onchange('fail_cause_id')
    def fail_cause_id_change(self):
        if not self.fail_cause_id:
            return
        fail_cause_id = self.fail_cause_id.id
        descriptions = self.env['project.task.fail.description'].\
            search([('fail_cause_id','=',fail_cause_id)])
        description_ids = [desc.id for desc in descriptions]
        return {'domain':{
            'fail_description_id':[('id','in',description_ids)]}
            }

    def onchange_user_id(self, cr, uid, ids, user_id, context=None):
        vals = {}
        if user_id:
            vals['date_start'] = fields.datetime.now()
            user = self.pool.get('res.users').browse(cr, uid, [user_id])
            if user.employee_ids:
                vals['employee_id'] = user.employee_ids[0].id
            else:
                vals['employee_id'] = False
        return {'value': vals}

    def action_print_ot(self):
        pass

class ProductProduct(models.Model):
    _inherit = 'product.template'

    service_cat_id = fields.Many2one('product.service.category',
        string='Product Service Category')
    hide_service_category = fields.Boolean('Hide Service Category')

    @api.onchange('categ_id')
    def categ_id_onchange(self):
        if not self.categ_id:
            return
        if self.categ_id.is_metalworking:
            self.hide_service_category = True
        else:
            self.hide_service_category = False

class ProductServiceCategory(models.Model):
    _name = 'product.service.category'
    _description = 'Service Categories'

    name = fields.Char('Category Name', required=True)
    # system_id = fields.Char('System Id', required=True)
    product_category_id = fields.Many2one('product.category',
        string='Product Category', required=True)
    product_ids = fields.One2many('product.product','service_cat_id',
        string='Products')

    @api.model
    def name_search(self, name='', args=None, operator='ilike', limit=100):
        if not args:
            args = []
        if name:
            positive_operators = ['=', 'ilike', '=ilike', 'like', '=like']
            if operator in positive_operators:
                product_cat = self.env['product.category'].search([
                    ('name','ilike',name)])
                product_cat_ids = [pcat.id for pcat in product_cat]
                cats = self.search([
                    '|',
                    ('product_category_id', 'in', product_cat_ids),
                    ('name', operator, name)
                    ])
        else:
            cats = self.search(args, limit=limit)
        return cats.name_get()

    @api.multi
    def name_get(self):
        result = []
        for cat in self:
            name = '[%s] %s'%(cat.product_category_id.name, cat.name)
            result.append((cat.id, name))
        return result

class ProjectTaskServices(models.Model):
    _name = "project.task.service"
    _description = "Jobs Executed in Task"

    task_id = fields.Many2one(
        comodel_name='project.task', string='Task', ondelete='cascade',
        required=True)
    product_id = fields.Many2one(
        comodel_name='product.product', string='Product', 
        required=True, domain=[('type','=','service')])
    quantity = fields.Integer(string='Quantity', default=1)

    @api.multi
    @api.constrains('quantity')
    def _check_quantity(self):
        for service in self:
            if not service.quantity > 0:
                raise ValidationError(
                    _('Quantity of services executed must be greater than 0.')
                )

class ProjectCustomerAssets(models.Model):
    _name = "project.customer.asset"
    _description = "Customer Assets"

    task_id = fields.Many2one(
        comodel_name='project.task', string='Task', ondelete='cascade',
        required=True)
    product_id = fields.Many2one(
        comodel_name='product.product', string='Product', required=True)
    quantity = fields.Integer(string='Quantity')
    replaced = fields.Boolean('Reemplazado?', default=False)
    product_category_id = fields.Many2one('product.category',
        string='Product Category')
    product_service_cat_id = fields.Many2one('product.service.category',
        string='Product Service Category')
    equipment_id = fields.Many2one('bt.asset', string='Equipment')
    equipment_type_id = fields.Many2one('project.spare.equipment.type',
        string='Equipment Type')
    spare_location_id = fields.Many2one('project.equipment.spare.location',
        string='Spare Location')
    is_epoel = fields.Boolean('Is HE or LE?')
    #New Fields
    asset_location = fields.Text('Asset Location')
    #asset_image = fields.Text('Asset Photo')
    asset_image = fields.Binary('photo', attachment=True)
    task_date_start = fields.Datetime(related='task_id.date_start',
        string='Task Date')
    #REQ No. 140
    serial_number = fields.Char('Serial Number', required=True, default='N/A')
    placa = fields.Char('Placa', required=True, default='N/A')

    @api.multi
    @api.constrains('quantity')
    def _check_quantity(self):
        for asset in self:
            if not asset.quantity > 0.0:
                raise ValidationError(
                    _('Quantity of assets must be greater than 0.')
                )

    @api.multi
    @api.onchange('product_category_id')
    def system_id_change(self):
        if not self.product_category_id:
            return
        self.is_epoel = True if self.product_category_id\
            .is_metalworking else False
        return {
            'domain': {
                'product_service_cat_id': [('product_category_id','=',self.product_category_id.id)]
            }
        }

    @api.multi
    @api.onchange('product_service_cat_id')
    def systems_change(self):
        if not self.product_service_cat_id:
            return
        part_ids = self.env['product.product'].search(
            [('service_cat_id','=',self.product_service_cat_id.id)]
            )
        part_ids = [part.id for part in part_ids]
        return {'domain': {'product_id': [('id','in',part_ids)]}}

    @api.multi
    @api.onchange('spare_location_id')
    def spare_location_id_change(self):
        if not self.spare_location_id:
            return
        product_obj = self.env['product.template']
        category = self.product_category_id.id
        product_ids = []
        for product in product_obj.search([('categ_id','=',category)]):
            for cat_line in product.equipment_cat_ids:
                if cat_line.equipment_type.id == self.equipment_type_id.id \
                and cat_line.spare_location.id == self.spare_location_id.id:
                    product_ids.append(product.id)
        return {
            'domain': {
                'product_id': [('id','in',product_ids)]
            }
        }

# REQ No. 83
class MailComposeMessage(models.Model):
    _inherit = 'mail.compose.message'
    #Metodo que se acciona cuando se envia el correo desde el wizard
    @api.multi
    def send_mail(self, auto_commit=False):
        if self._context.get('default_model') == 'project.task' and self._context.get('default_res_id'):
            order = self.env['project.task'].browse([self._context['default_res_id']])
            if order.state == 'valid':
                order.state = 'notif'
        return super(MailComposeMessage, self.with_context(mail_post_autofollow=True)).send_mail(auto_commit=auto_commit)
# END REQ No. 83