# -*- coding: utf-8 -*-

from openerp import fields, models

class EmployeeExpense(models.Model):
    _inherit = 'hr.expense'

    task_id = fields.Many2one('project.task', string='Task')