# -*- coding: utf-8 -*-

from openerp import fields, models
#from openerp.exceptions import ValidationError
import logging

_logger = logging.getLogger(__name__)

class BtAsset(models.Model):
    _inherit = 'bt.asset'

    repair_ids = fields.One2many('project.customer.asset','equipment_id',
        string='Interventions')