# -*- coding: utf-8 -*-

from openerp import fields, models

class StockLocation(models.Model):
    _inherit = 'stock.location'

    material_source_location = fields.Boolean('Default Source Location',
        default=False)
    material_target_location = fields.Boolean('Default Target Location',
        default=False)