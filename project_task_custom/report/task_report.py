from openerp import api, models
from datetime import datetime
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT as DTF
import pytz
import logging

_logger = logging.getLogger(__name__)

#REQ #23
def str_to_datetime(strdate):
    try:
        return datetime.strptime(strdate, DTF)
    except Exception as e:
        return False

class TaskReport(models.AbstractModel):
    _name = 'report.project_task_custom.task_report'
    
    @api.model
    def render_html(self, docids, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('project_task_custom.task_report')
        docargs = {
            'doc_ids': docids,
            'doc_model': report.model,
            'docs': self.env['project.task'].search([('id','in',docids)]),
            'get_subsystems': self._get_subsystems,
            'get_subsystem_products': self._get_subsystem_products,
        }
        return report_obj.render('project_task_custom.task_report', docargs)

    def _get_subsystems(self, assets):
        ids = []
        res = []
        for asset in assets:
            if asset.product_service_cat_id not in ids:
                ids.append(asset.product_service_cat_id)
                tup = (asset.product_category_id.name, 
                    asset.product_service_cat_id.name)
                res.append(tup)
        return res

    def _get_subsystem_products(self, assets):
        res = []
        for asset in assets:
            #if asset.product_id.name not in res:
            res.append(asset.product_id)
        return res