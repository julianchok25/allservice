# -*- coding: utf-8 -*-
from openerp import models, fields, api


class crm_lead(models.Model):

    _inherit = 'crm.lead'
    _description = 'Campos nuevos en el formulario de oportunidades'

    #Campo para asignar categorías como tags en la oportunidad 
    categs_ids = fields.Many2many(comodel_name='product.category',
                                  relation='crm_lead_categ_rel',
                                  column1='crm_lead_id',
                                  column2='product_category_id',
                                  string='Categorias *',
                                  help='Puede agregar varias categorias')

    siplaft_id = fields.Many2one('crm.siplaft', string="Siplaft")

    tipo_opor = fields.Selection([
        ('inst', "Instalación"),
        ('mtto', "Mantenimiento"),
        ('supply', "Suministro"),
        ('transfer', "Traslado")
    ], default='inst', required=True)

    #Se desactiva campo requerido para el nombre en la oportunidad
    name = fields.Char(required=False)
    partner_id = fields.Many2one('res.partner', required=True)
    street = fields.Char(required=True, string="Dirección *")
    mobile = fields.Char(required=True)
    email_from = fields.Char(required=True)
    city = fields.Char(required=True)
    city_id = fields.Many2one('res.better.zip', string='City')

    sec = fields.Char(string='Oportunidad No.', required=True, copy=False,
            readonly=True, index=True, default=lambda self: ('Nuevo'))
    phone_ext = fields.Integer('Extension')

    #Función heredada que crea la secuencia
    @api.model
    def create(self, vals):
        if vals.get('sec', 'Nuevo') == 'Nuevo':
            vals['sec'] = self.env['ir.sequence'].next_by_code('crm.lead')
        return super(crm_lead, self).create(vals)
    
    #Campo que trae la Identificación del cliente (persistencia)
    vat_ref = fields.Char(related='partner_id.vat', string="IDCliente", readonly=True)

    # Función que autocompleta los demas campos de la locación
    @api.one
    @api.onchange('city_id')
    def _onchange_location(self):
        if self.city_id:
            self.zip = self.city_id.name
            self.city = self.city_id.city
            self.state_id = self.city_id.state_id
            self.country_id = self.city_id.country_id
    
    # @api.model
    # @api.multi
    # def action_set_lost(self):
    #     res = super(crm_lead, self).action_set_lost()
    #     stages_leads = {}
    #     vals = {}
    #     for lead in self.browse():
    #         vals['stage_id'] = self.stage_find([lead], lead.team_id.id or False, [('probability', '=', 0.0), ('on_change', '=', True)])
    #         if stage_id:
    #             if stages_leads.get(stage_id):
    #                 vals['stages_leads[stage_id]'].append(lead.id)
    #             else:
    #                 vals['stages_leads[stage_id]'] = [lead.id]
    #     for stage_id, lead_ids in stages_leads.items():
    #         self.create(lead_ids, {'stage_id': stage_id})
    #         #self.create(lead_ids, {'stage_id': stage_id})
    #     # vals = {}
    #     # vals['stages_leads'] = stages_leads
    #     # vals['stage_id'] = stage_id
    #     # vals['lead_ids'] = lead_ids
    #     self.update(vals)
    #     # self.update(lead_ids)
    #         # return self.write({'probability': 0})
    #     # self.write({'probability': 0})
    
    #     return res

