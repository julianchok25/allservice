# -*- coding: utf-8 -*-

from openerp import models, fields


class res_partner(models.Model):

    _inherit = 'res.partner'
    _description = 'Modelo para realizar cambios en el contacto o partner'

    company_type = fields.Selection(
            selection=[('person', 'Natural'),
                       ('company', 'Jurídica')])
    location_manager = fields.Boolean('Location Manager', \
        default=False)