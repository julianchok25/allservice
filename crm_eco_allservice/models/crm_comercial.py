# -*- coding: utf-8 -*-

from openerp import api, fields, models
import logging

_logger = logging.getLogger(__name__)

class CrmProspectus(models.Model):
    _name = 'crm.prospectus'
    _description = 'CRM Prospectus'

    name = fields.Char('Name', required=True,
        default='New')
    lead_id = fields.Many2one('crm.lead', 'Lead ID')
    area_protect = fields.Integer('Area Protect', default=1)
    area_ids = fields.One2many('crm.areas', 'prospectus_id', 'Areas')
    place_implementation = fields.Selection(
        [('residencial','Residencial'),
         ('comercial','Comercial'),
         ('empresarial','Empresarial')
        ],'Place Imp'
        )
    monitor_system_ids = fields.Many2many('crm.monitoring.system',
        string='Monitoring Systems')
    channel_ucp = fields.Selection(
        [('x4ch','x4CH'),
         ('x8ch','x8CH'),
         ('x12ch','x12CH'),
         ('x16ch','x16CH')
        ],'Channel UCP'
        )
    is_monitor_system = fields.Boolean('Monitoring System?')
    attc_file = fields.Binary('Attachment File', attachment=True)
    attc_file_ch = fields.Char('Attachment File Char')
    customer_need_id = fields.Many2one('product.category', 
        string='Customer Needs')
    amount_money = fields.Integer('Amount of Money to Store')
    equipment_type_ids = fields.Many2many('project.spare.equipment.type', 
        string='Type of Equipment')
    other_et = fields.Char('Other Et')
    necessary_el_ids = fields.Many2many('crm.elements.light.eqm',
        string='Elements')
    other_ne = fields.Char('Other Ne')
    color = fields.Selection([
        ('gris', 'Gris'),
        ('blanco', 'Blanco'),
        ('negro', 'Negro'),
        ('cafe', 'Cafe'),
        ('otro', 'otro'),
        ], string='Color')
    other_color = fields.Char('Other Color')
    front = fields.Integer('Front')
    fund = fields.Integer('Fund')
    high = fields.Integer('High')
    value_type_ids = fields.Many2many('crm.value.type', string="Type of Value to Store")
    other_vt = fields.Char('Others')
    is_armor_eqm = fields.Boolean('Is Armor')
    blind_level = fields.Selection([
        ('lvl1', 'Level 1'),
        ('lvl2', 'Level 2'),
        ('lvl3', 'Level 3'),
        ], string='Armor Level')
    #Control de Acceso
    qty_access = fields.Integer('Number of Accesses')
 

class CrmAreas(models.Model):
    _name = 'crm.areas'
    _description = 'CRM Areas'

    name = fields.Char('Name', required=True,
        default='New')
    prospectus_id = fields.Many2one('crm.prospectus', 
        'Prospectus ID')
    sensors_total = fields.Integer('Sensors', default=0)
    wall_type = fields.Char('Wall Type')
    door_type = fields.Char('Door Type')
    proximity = fields.Integer('Proximity', default=0)
    max_height = fields.Integer('Max Height')
    description = fields.Text('Description')
    photo = fields.Binary('Foto', attachment=True)
    photo_ids = fields.Many2many('ir.attachment', string='Photos', attachment=True)
    muk_ids = fields.Many2many('muk_dms.file', string='Photos Muk')
    #Control de acceso
    entry_room_ids = fields.Many2many('crm.entry.room', string='Entrance of Rooms')
    exit_room_ids = fields.Many2many('crm.exit.room', string='Departure of Rooms')


class CrmMonitoringSystem(models.Model):
    _name = 'crm.monitoring.system'
    _description = 'CRM Monitoring System'

    name = fields.Char('Name', required=True,
        default='New')
    description = fields.Char('Description')
    #prospectus_id = fields.Many2one('crm.prospectus', 'Prospectus ID')


class CrmLead(models.Model):
    _inherit = 'crm.lead'

    @api.one
    @api.depends('prospectus_ids')
    def _prospectus_count(self):
        # _logger.critical('POR EL COUNT2')
        # self.prospectus_count = 1
        if self.prospectus_ids:
            self.prospectus_count = len(self.prospectus_ids)
        else:
            self.prospectus_count = 0

    prospectus_ids = fields.One2many('crm.prospectus','lead_id', string='Prospectus')
    prospectus_count = fields.Integer('Prospectus Count', compute='_prospectus_count')

class CrmElements(models.Model):
    _name = 'crm.elements.light.eqm'
    _description = 'crm.elements.light.eqm'

    name = fields.Char('name', required=True)


class CrmValueType(models.Model):
    _name = 'crm.value.type'
    _description = 'crm.value.type'

    name = fields.Char('name', required=True)

class CrmEntryRoom(models.Model):
    _name = 'crm.entry.room'
    _description = 'Entrada de habitaciones'

    name = fields.Char('name',  required=True)
    category_id = fields.Many2one('product.category','Category')

class CrmExitRoom(models.Model):
    _name = 'crm.exit.room'
    _description = 'Salida de habitaciones'

    name = fields.Char('name', required=True)
    category_id = fields.Many2one('product.category','Category')

class MukDocuments(models.Model):
    _inherit = 'muk_dms.file'
    _description = 'Documents'

    prospectus_id = fields.Many2one('crm.prospectus')
    areas_id = fields.Many2one('crm.areas','Areas')
