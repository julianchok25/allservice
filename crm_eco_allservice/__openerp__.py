# -*- coding: utf-8 -*-
{
    'name': "crm_eco_allservice",

    'summary': """
       El propósito de este módulo es ajustar el proceso comercial y
       personalizarlo para all service""",

    'description': """
        Se utiliza cuando se levantarán nuevos clientes o prospectos,
        Se valida el Siplaf, costo del proyecto y disponibilidad.
    """,

    'author': "ITSolucines S.A.S ",
    'website': "http://itsoluciones.net/",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module
    #/module_data.xml
    # for the full list
    'category': 'Allservice',
    'version': '9.0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'crm', 'bt_asset_management_custom'],

    # always loaded
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'security/ir.model.access.csv',
        'views/crm_lead_view.xml',
        'views/number_sequence.xml',
        'views/templates.xml',
        'views/siplaft_view.xml',
        'views/siplaft_workflow.xml',
        'views/ficha_prospecto_view.xml',
        'views/res_partner_view.xml'
    ],
    'css': ['static/src/css/custom_style.css'

    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True
}