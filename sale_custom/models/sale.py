# -*- coding: utf-8 -*-

from openerp import fields, models, api
from datetime import datetime
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT as DTF

# REQ #12
months = {
    1: 'Enero', 2: 'Febrero', 3: 'Marzo',   4: 'Abril',
    5: 'Mayo',  6: 'Junio', 7: 'Julio', 8: 'Agosto',
    9: 'Septiembre',    10: 'Octubre',  11: 'Noviembre',
    12: 'Diciembre'
    }

class SaleOrder(models.Model):
    _inherit = "sale.order"

    def _date_letters(self):
        now = datetime.now()
        try:
            return '%s %s de %s'%(months[now.month], now.day, now.year)
        except:
            pass
    
    @api.one
    @api.depends('opportunity_id')
    def _compute_note(self):
        for r in self:
            if r.opportunity_id.name:
                oppor_upper = r.opportunity_id.name.upper()
            else:
                oppor_upper = "(TIPO Y LOCACION)"
            r.body_report = u"""
                <div style="font-family: Arial; font-size: 14px; color: rgb(0, 0, 0); background-color: rgb(255, 255, 255);">
                    <p>Respetado(a) Señor(a)</p><br/> 
                    <p>A continuación presentamos la cotización por <span><b>%s</b></span> 
                    en la oficina <b>(NOMBRE OFICINA)</b></p>
                    <p>Cualquier inquitud sobre los sistemas propuestos, la atenderemos lo más pronto
                    posible a través del departamento de ingeniería.</p>
                    <p>Por último, describimos las condiciones comerciales de la oferta para su evaluación. </p> 
                        
                </div>""" %(oppor_upper)
                #%(r.opportunity_id.name)
    
    # Otra manera de agregar texto con la funcion default
    def _get_default_terms(self):
        result = u"""
            <div style="font-family: Arial; font-size: 14px; color: rgb(0, 0, 0); background-color: rgb(255, 255, 255);">
                <div class="text-center"><b>CONDICIONES COMERCIALES</b></div>
                <div><b>NOTAS:</b></div> 
                <p>- Los precios que anteriormente se cotizan están dados en pesos colombianos.</p>
                <p>- El impuesto a las ventas (IVA) será calculado en la facturación de acuerdo con los 
                índices vigentes a la fecha.</p>
                <p>- Los elementos y/o mano de obra adicionales que se requieran y no estén contemplados
                en esta propuesta, se cobrarán al finalizar la obra.</p></br>
                <div><b>VALIDEZ DE LA OFERTA:</b></div>
                <p>La validez de la oferta es de treinta días(30), contados a partir de la fecha.</p>
                <div><b>FORMA DE PAGO:</b></div>
                <p>Contra entrega a satisfacción del cliente.</p>
                <div><b>TIEMPO DE ENTREGA:</b></div> 
                <p>A convenir.</p>
                <div><b>GARANTIA:</b></div>
                <p>La garantía será de un (1) año contado a partir de la fecha de entrega de los equipos.
                Contra defectos de fabricación, la garantía no será efectiva sobre viltajes o manejo incorrecto
                de los equipos.</p>                     
            </div>""" 
        return result
   

    city_id = fields.Many2one("res.better.zip", \
        string="Ciudad", required=True)
    cdate = fields.Char('Fecha', default=_date_letters, store=True)
    body_report =  fields.Html('Body Report', compute=_compute_note, readonly=False)
    terms_conds = fields.Html('Terms and Conditions', default=_get_default_terms)
# REQ #12 FIN

class SaleOrder(models.Model):
    _inherit = "res.company"
    # Se hereda el modelo de la compania para modificar la regla de seguridad