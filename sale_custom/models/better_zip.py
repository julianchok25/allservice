# -*- coding: utf-8 -*-

from openerp import models, fields, api

# REQ #12
class BetterZip(models.Model):
    _inherit = "res.better.zip"
    _rec_name = "display_name"

    @api.one
    @api.depends(
        'name',
        'city',
        'state_id',
        'country_id',
        )
    def _get_display_name(self):
        name = [self.city]
        if self.state_id:
            name.append(self.state_id.name)
        if self.country_id:
            name.append(self.country_id.name)
        if self.name:
            name.append(self.name)
        self.display_name = ", ".join(name)
# REQ #12 FIN