# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    John W. Viloria Amaris <john.viloria.amaris@gmail.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    "name":"Crm Customer Customizations",
    "description":"""\
Crm Customer Customizations
==================================

Modificaciones varias al módulo de CRM.
    """,
    "depends":['crm',],
    "category":"CRM",
    "author":"John Viloria Amaris",
    "data":[
    	"security/ir.model.access.csv",
        "views/crm_customer_view.xml",
    ],
    "installable":True
 }
