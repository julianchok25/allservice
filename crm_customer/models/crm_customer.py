# -*- coding: utf-8 -*-

from openerp import api, fields, models, _
from openerp.exceptions import ValidationError
import logging

_logger = logging.getLogger(__name__)

class CrmCustomerLocation(models.Model):
    _name = 'crm.customer.location'
    _description = 'Equipment'

    name = fields.Char('Location Name', required=True)
    longitude = fields.Char('Longitude')
    latitude = fields.Char('Latitude')
    city_id = fields.Many2one('res.better.zip', string='City')
    partner_id = fields.Many2one('res.partner', string='Partner Owner', \
        required=True)
    location_type_id = fields.Many2one('crm.customer.location.type', \
        string='Location Type', required=True)
    code = fields.Char('Code', required=True)
    region = fields.Selection([
        ('ant', "Antioquia"),
        ('bgta', "Bogota y Cundinamarca"),
        ('caribe', "Caribe"),
        ('centro', "Centro"),
        ('sur', "Sur")
    ], default='', required=False, string="Region")
    address = fields.Char('Address')

    @api.model
    def name_search(self, name='', args=None, operator='ilike', limit=100):
        if not args:
            args = []
        if name:
            positive_operators = ['=', 'ilike', '=ilike', 'like', '=like']
            if operator in positive_operators:
                locations = self.search([
                    '|',
                    ('code', operator, name),
                    ('name', operator, name)
                    ])
        else:
            locations = self.search(args, limit=limit)
        return locations.name_get()

    @api.multi
    def name_get(self):
        result = []
        for location in self:
            name = '[%s] %s'%(location.code, location.name)
            result.append((location.id, name))
        return result

    @api.constrains('partner_id', 'code','location_type_id')
    def _check_unicity(self):
        if self.search_count([
                ('partner_id', '=', self.partner_id.id),
                ('code', '=', self.code),
                ('location_type_id', '=', self.location_type_id.id)
            ]) > 1:
            raise ValidationError(_("Partner location duplicated!"))

class CrmBranchOffice(models.Model):
    _name = 'crm.customer.location.type'
    _description = 'Office Building'

    name = fields.Char('Name', required=True)
    # partner_id = fields.Many2one('res.partner', string='Partner Owner')
    # city_id = fields.Many2one('res.better.zip', string='City')