# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    John W. Viloria Amaris <john.viloria.amaris@gmail.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    "name":"Project Issue Customizations",
    "description":"""\
Project Issue Customizations
==================================

Modificaciones varias al módulo de Incidencias.
    """,
    "depends":['project_issue','crm_customer','crm_eco_allservice'],
    "category":"Project",
    "author":"John Viloria Amaris",
    "data":[
    	"views/project_issue_view.xml",
        "views/number_sequence.xml",
        "views/project_issue_fail_view.xml",
        "security/ir.model.access.csv",
        "views/res_users_view.xml",
        "views/res_partner_view.xml",
        "views/templates.xml",
        "views/project_view.xml",
    ],
    'css': ['static/src/css/issue_custom.css'], 
    "installable":True
 }
