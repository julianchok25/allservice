# -*- coding: utf-8 -*-

from datetime import timedelta
from openerp import api, fields, models, _
from openerp.exceptions import ValidationError
import logging

_logger = logging.getLogger(__name__)

class ProjectIssue(models.Model):
    _inherit = "project.issue"

    # Asigna por defecto el valor de oficina para el campo Tipo de Ubicacion
    @api.model
    def _default_location_type(self):
        return self.env['crm.customer.location.type'].search([('name','=','Oficina')],limit=1)
    
    # Asigna por defecto el valor del cliente, teniendo en cuenta la asociacion del proyecto
    @api.model
    def _default_partner_issue(self):
        project_id = self.env.context.get('default_project_id', False)
        partner_id = False
        if project_id:
            try:
                partner_id = self.env['project.project'].\
                    search([('id','=',project_id)])[0].partner_id.id
            except:
                pass
        else:
            try:
                partner_id = self.env['project.project'].\
                    search([('project_default','=', True)])[0].partner_id.id
            except:
                partner_id = self.env['project.project'].\
                    search([])[0].partner_id.id
        return partner_id
    
    categs_ids = fields.Many2many('product.category',
                                  string='Categorias',
                                  help='Puede agregar varias categorias')
    request_type = fields.Selection([
        ('macor', "Mantenimiento Correctivo"),
        ('mapreven', "Mantenimiento Preventivo")
    ], default='macor', required=True, string="Tipo")
    
    # Julian
    branch_type = fields.Selection([
        ('office', "Oficina"),
        ('atm', "Cajero"),
        ('edi', "Edificio")
    ], default='office', required=False, string="Locacion")
    
    #Se desactiva campo requerido para el nombre en la incidencia
    name = fields.Char(required=False)
    request_source = fields.Selection([
        ('sap','SAP Request'),
        ('internal','Internal Request'),
        ('special','Special Project'),
        ], default='sap', string='Request Origin', required=True)
    #Campo que se debe activar si es un codigo SAP
    number_sap = fields.Char(string='Numero de Servicio', required= False, copy= False)
    contact_id = fields.Many2one('res.partner', string='Contact')
    city_id = fields.Many2one('res.better.zip', string='City',\
        required=True)
    location_type_id = fields.Many2one('crm.customer.location.type', default=_default_location_type, \
        string='Location Type')
    location_id = fields.Many2one('crm.customer.location',\
        string='Codigo', required=True)
    date_start = fields.Date('Date Start', default=fields.Date.today())
    date_assign = fields.Datetime('Date of Assignment', default=fields.Datetime.now())
    date_finish = fields.Date('Date Finish', compute='_get_end_date', readonly=False)

    sec = fields.Char(string='Ref.', required=True, copy=False,
        readonly=True, index=True, default=lambda self: ('Nuevo'))
    assignment_status = fields.Selection([
        ('fail', 'Fail'),
        ('cancel', 'Cancelled')
        ], string='Assignment Status')
    fail_cause_id = fields.Many2one('project.task.fail.cause',\
        string='Fail Cause')                #ELIMINAR
    fail_description_id = fields.Many2one('project.task.fail.description',\
        string='Fail Description')          #ELIMINAR
    fail_notes = fields.Text('Notes')       #ELIMINAR
    ro_project_id = fields.Many2one('project.project', string='Project')
    partner_id = fields.Many2one(domain=[
        ('is_company','=',True),
        ('customer','=',True)
        ], default=_default_partner_issue)
    #REQ No. 113
    user_id = fields.Many2one(domain=[('coordinator','=',True)])

    task_count = fields.Integer('Task Count', compute='_task_count',
        default=0)
    task_ids = fields.One2many('project.task', 'issue_id', \
        string='Tasks')

    @api.depends('task_ids')
    def _task_count(self):
        if self.task_ids:
            self.task_count = len(self.task_ids)
        else:
            self.task_count = 0

    #Funcion heredada que crea la secuencia
    @api.model
    def create(self, vals):
        if vals.get('project_id', False):
            vals['ro_project_id'] = vals['project_id']
        if vals.get('sec', 'Nuevo') == 'Nuevo':
            vals['sec'] = self.env['ir.sequence'].next_by_code('project.issue')
        if vals.get('request_source', False) == 'internal':
            vals['number_sap'] = self.env['ir.sequence']\
                .next_by_code('project.issue.internal.request')
        if vals.get('number_sap', False):
            vals['name'] = '%s %s'%(vals['name'], vals['number_sap'])
        return super(ProjectIssue, self).create(vals)

    @api.multi
    def write(self, vals):
        if 'project_id' in vals:
            vals['ro_project_id'] = vals['project_id']
        return super(ProjectIssue, self).write(vals)

    def _days_skip(self, days):
        start = fields.Datetime.from_string(self.date_start)
        cnt = 0
        for x in range(0, days):
            d = start + timedelta(days=x)
            if d.strftime('%A').lower() in ['sabado','saturday','sunday', 'domingo']:
                cnt += 1
        return cnt

    #REQ No. 130
    @api.one
    @api.depends('date_start','request_type')
    def _get_end_date(self):
        if self.request_type == 'mapreven':
            try:
                start = fields.Datetime.from_string(self.date_start)
                duration = timedelta(days=30)
                days_skip = timedelta(days=self._days_skip(30))
                self.date_finish = start + (duration + days_skip)
            except:
                pass
        elif self.request_type == 'macor':
            try:
                start = fields.Datetime.from_string(self.date_start)
                days_skip = timedelta(days=self._days_skip(5))
                self.date_finish = start + (timedelta(days=5) + days_skip)
            except:
                pass
            
    @api.multi
    @api.onchange('location_type_id','partner_id', 'city_id')
    def location_id_change(self):
        if not self.location_type_id and not self.partner_id and \
            not self.city_id:
            return
        domain = []
        if self.partner_id:
            domain.append(('partner_id','=',self.partner_id.id))
        if self.location_type_id:
            domain.append(('location_type_id','=',self.location_type_id.id))
        if self.city_id:
            domain.append(('city_id','=',self.city_id.id))
        if len(domain) > 1:
            domain = ['&'] + domain
        locations = self.env['crm.customer.location'].\
            search(domain, limit=100)
        location_ids = self.city_filter(locations) #Retorna el Id de la Ciudad
        ids = [location.id for location in locations]
        return {'domain':{
            'location_id':[('id','in',ids)],
            'city_id':[('id','in',location_ids)]}
            }

    def city_filter(self, locations):
        city_ids = []
        if locations:
            city_ids = [location.city_id.id for location in locations]
        return city_ids

    @api.multi
    def real_location_id_change(self, location_id):
        if not location_id:
            return
        res_dict = {}
        location_id = self.env['crm.customer.location'].\
            search([('id','=',location_id)])
        contacts = self.env['res.partner']\
            .search([('parent_id','=',location_id.partner_id.id)])
        contact_ids = [contact.id for contact in contacts]
        res_dict['domain'] = {'contact_id':[('id','in',contact_ids)]}
        res_dict['value'] = {'city_id': location_id.city_id.id,
             'location_type_id': location_id.location_type_id.id,
             'partner_id': location_id.partner_id.id}
        if len(contact_ids) == 1:
            res_dict['value']['contact_id'] = contact_ids[0]
        return res_dict
    
    # Restriction number_sap Unique
    @api.constrains('number_sap')
    def _check_unique_number(self):
        if self.search_count([
                ('number_sap', '=', self.number_sap)
            ]) > 1:
            raise ValidationError(_("Notice Number duplicated!"))
    
    # Use this method if number_sap is required in your attribute field
    # @api.one
    # def copy(self, default=None):
    #     if default is None:
    #         default= {}
    #     new_number = (self.number_sap + ' (copy)') if self.number_sap else ''
    #     # default['number_sap'] = new_number
    #     default.update({
    #         'number_sap': new_number,
    #     })
    #     return super(ProjectIssue, self).copy(default)

    # @api.multi
    # @api.onchange('assignment_status')
    # def assignment_status_change(self):
    #     if not self.assignment_status:
    #         return
    #     assignment_status = self.assignment_status
    #     fails = self.env['project.task.fail.cause'].\
    #         search([('assignment_status','=',assignment_status)])
    #     fail_ids = [fail.id for fail in fails]
    #     return {'domain':{
    #         'fail_cause_id':[('id','in',fail_ids)]}
    #         }        

    # @api.multi
    # @api.onchange('fail_cause_id')
    # def fail_cause_id_change(self):
    #     if not self.fail_cause_id:
    #         return
    #     fail_cause_id = self.fail_cause_id.id
    #     descriptions = self.env['project.task.fail.description'].\
    #         search([('fail_cause_id','=',fail_cause_id)])
    #     description_ids = [desc.id for desc in descriptions]
    #     return {'domain':{
    #         'fail_description_id':[('id','in',description_ids)]}
    #         }

    @api.multi
    def name_project_issue_change(self,request_type=None,categs_ids=None,\
        location_type_id=None,city_id=None,partner_id=None, location_id=None):
        res = self.set_name_issue(request_type,categs_ids,\
            location_type_id,city_id,partner_id,location_id)
        if res: return {'value':res}

    @api.multi
    def partner_id_change(self, request_type=None, categs_ids=None, \
        location_type_id=None, city_id=None, partner_id=None):
        if not partner_id:
            return
        res_dict = {}
        partner = self.env['res.partner'].search(
            [('id','=',partner_id)])
        contacts = self.env['res.partner']\
            .search([('parent_id','=',partner_id)])
        contact_ids = [contact.id for contact in contacts]
        res_dict['domain'] = {'contact_id':[('id','in',contact_ids)]}
        res = self.set_name_issue(request_type,categs_ids,\
            location_type_id,city_id,partner_id)
        if res:
            res_dict['value'] = res
        if contact_ids:
            if len(contact_ids) == 1:
                res_dict['value']['contact_id'] = contact_ids[0]
        else:
            res_dict['value']['contact_id'] = False
        res_dict['value']['project_id'] = partner.project_id.id
        res_dict['value']['ro_project_id'] = partner.project_id.id
        return res_dict

    def set_name_issue(self,request_type=None,categs_ids=None,\
        location_type_id=None,city_id=None,partner_id=None, location_id=None):
        if not request_type and not categs_ids \
            and not partner_id and not location_type_id \
            and not city_id:
            return
        name = []
        if request_type:
            request_type_name = dict(self._columns['request_type'].selection)\
                .get(request_type)
            name.append(request_type_name)
        # if categs_ids:
        #     try:
        #         categs_ids = categs_ids[0][2]
        #     except:
        #         categs_ids = []
        #     for cat in self.env['product.category'].search(\
        #         [('id','in',categs_ids)]):
        #         name.append(cat.name)
        # if partner_id:
        #     partner = self.env['res.partner'].search([('id','=',partner_id)])
        #     name.append(partner.name)
        if location_type_id:
            location_type = self.env['crm.customer.location.type'].search(\
                [('id','=',location_type_id)])
            name.append(location_type.name)
        if location_id:
            location = self.env['crm.customer.location'].\
            search([('id','=',location_id)])
            name.append(location.code)
        if city_id:
            city = self.env['res.better.zip'].search(\
                [('id','=',city_id)])
            name.append(city.city)
        if len(name) > 0:
            clean_name(name)
            name = ' '.join(name)
            return {'name': name}

def clean_name(name):
    if len(name) > 0:
        try:
            name.remove(False)
        except:
            pass
        try:
            name.remove(None)
        except:
            pass


class ProjectTaskFailCause(models.Model):
    _name = 'project.task.fail.cause'

    name = fields.Char('Name', required=True)
    assignment_status = fields.Char('Assignment Status', required=True)

class ProjectFaskFailDescripion(models.Model):
    _name = 'project.task.fail.description'

    name = fields.Char('Name', required=True)
    fail_cause_id = fields.Many2one('project.task.fail.cause',\
        string='Fail Cause')

class ResUsers(models.Model):
    _inherit = "res.users"

    technician = fields.Boolean('Is Technician?', default=False)
    salesman = fields.Boolean('Is Salesman?', default=False)
    player_id = fields.Char('ID Notification')
    #RE No. 113
    coordinator = fields.Boolean('Is Coordinator?', default=False) #END REQ No. 113

class ResPartner(models.Model):
    _inherit = 'res.partner'

    project_id = fields.Many2one('project.project',
        string='Default Project')

class Project(models.Model):
    _inherit = 'project.project'

    project_default = fields.Boolean('Default Project', default=False)