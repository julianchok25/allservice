# -*- coding: utf-8 -*-

from openerp import fields, models

class ProjectTask(models.Model):
    _inherit = 'project.task'

    issue_id = fields.Many2one('project.issue',
        string='Project Issue')